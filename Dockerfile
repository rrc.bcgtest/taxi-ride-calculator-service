# Use the official lightweight Node.js 14 image.
# https://hub.docker.com/_/node
FROM node:14-slim

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY package*.json ./
COPY tsconfig.build*.json ./

# Install dependencies.
RUN npm install

# Copy local code to the container image.
COPY . ./
RUN ls -ls
RUN npm run build

ENV PORT=8080

# Run the web service on container startup.
CMD ["node", "dist/main.js"]