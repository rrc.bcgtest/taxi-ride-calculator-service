import { Body, Controller, Get, HttpException, HttpStatus, Logger, Param, ParseIntPipe, Post, Res } from '@nestjs/common';
import { AppService } from '../services/app.service';
import { FareCalculatorService } from '../services/fare-calculator.service';
import { Ride } from '../interfaces/ride.interface';
import * as moment from 'moment-timezone';
import { Fare } from 'src/interfaces/fare.interface';
import { Response } from 'express';


@Controller('/v1/')
export class AppController {
  private readonly logger = new Logger(AppController.name);
  
  constructor(private readonly appService: AppService, private readonly fareCalculator: FareCalculatorService) { }

  @Get('/rides')
  async getAllRides(): Promise<void | Ride[]> {
    try {
      return this.appService.getAllRides();
    } catch (error) {
      this.logger.error(`Erron in getAllRides ${error}`);
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('/rides/:id')
  async getRideById(@Param('id', ParseIntPipe) id: number): Promise<void | Ride> {
    try {
      return this.appService.getRideById(id);
    } catch (error) {
      this.logger.error(`Erron in getRideById ${error}`);
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('/rides/start')
  async startRide(): Promise<void | Ride> {
    try {
      return this.appService.startRide();
    } catch (error) {
      this.logger.error(`Erron in Ride start ${error}`);
      throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Post('/rides/fare/calculate')
  calculateFare(@Body() ride: Ride, @Res() res: Response): any {
    return res.status(HttpStatus.OK).json(this.fareCalculator.calculateFare(ride));
  }

}
