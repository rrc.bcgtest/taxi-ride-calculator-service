import { Injectable } from '@nestjs/common';
import { Ride } from '../interfaces/ride.interface';
import * as moment from 'moment-timezone';
import * as idGenerator from 'nodejs-unique-numeric-id-generator';
import { RideRepositoryService } from 'src/repositories/ride-repository.service';

@Injectable()
export class AppService {
  rideObject: Ride;

  constructor(private readonly rideModel: Ride, private readonly rideRepository: RideRepositoryService) { }

  private readonly TIME_ZONE: string = 'Europe/Paris';
  private rides: Ride[] = [];

  /**
   * get all Rides
   * @returns 
   */
  getAllRides(): Promise<Ride[]> {
    return this.rideRepository.getAllRides();
  }

  /**
   * get ride by id
   * @param id 
   * @returns 
   */
  getRideById(id: number): Promise<Ride> {
    return this.rideRepository.getRideById(id);
  }

  /**
   * start a new Ride
   * @returns 
   */
  startRide(): Promise<Ride> {
    this.rideObject = new Ride();
    this.rideObject.id = Number(idGenerator.generate(new Date().toJSON()));
    this.rideObject.startTime = moment().tz(this.TIME_ZONE).format();
    this.rideObject.distance = this.randomBetween(1, 20);
    this.rideObject.duration = this.randomBetween(5, 30);
    return this.rideRepository.createRide(this.rideObject);
  }

  /**
   * random number for ride test data creation
   * @param min 
   * @param max 
   * @returns 
   */
  private randomBetween(min: number, max: number) {
    return Math.floor(
      Math.random() * (max - min + 1) + min
    )
  }


}
