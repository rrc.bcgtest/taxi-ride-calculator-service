import { Test, TestingModule } from '@nestjs/testing';
import { FareCalculatorService } from './fare-calculator.service';

describe('FareCalculatorService', () => {
  let service: FareCalculatorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FareCalculatorService],
    }).compile();

    service = module.get<FareCalculatorService>(FareCalculatorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
