import { Injectable, Logger } from '@nestjs/common';
import * as moment from 'moment-timezone';
import { Fare } from 'src/interfaces/fare.interface';
import { Ride } from 'src/interfaces/ride.interface';


@Injectable()
export class FareCalculatorService {
    private readonly logger = new Logger(FareCalculatorService.name);
    private readonly TIME_ZONE: string = 'Europe/Paris';

    /**
   * get the initial value for Ride
   * @returns 
   */
    getInitialCharge(): number {
        return 1;
    }

    /**
     * calculate fare for a ride
     * @param distance 
     * @param duration 
     * @param startTime 
     * @returns 
     */
    calculateFare(ride: Ride): Fare {
        this.logger.log(`Calculating Fare for Ride id:${ride.id}, distance:"${ride.distance}", duration:"${ride.duration}", startTime:"${ride.startTime}"`);
        const initialCharge: number = this.getInitialCharge();
        let charge: number = 0;
        let rideDurationInHours = (ride.duration / 3600);

        let rideTime: any = moment(ride.startTime).tz(this.TIME_ZONE);
        this.logger.log(`Ride Started at : ${rideTime}`);
        charge = (initialCharge + (ride.distance / (0.2)) * this.getPricePerFifthMile(rideTime));

        let fare: Fare = new Fare();
        fare.ride = ride;
        fare.price = charge;

        return fare;
    }

    /**
     * calculate the price per fifth mile according to the time of the ride
     * @param rideTime 
     * @returns 
     */
    private getPricePerFifthMile(rideTime: any): number {
        let pricePerFifthMile: number = 0.5;
        if ((rideTime.hour() > 16 && rideTime.minute() >= 0) || (rideTime.hour() < 19 && rideTime.minute() >= 0)) {
            pricePerFifthMile = (pricePerFifthMile + 1);
            this.logger.log(`Busy period pricePerFifthMile: ${pricePerFifthMile}`);
        }
        else if ((rideTime.hour() > 20 && rideTime.minute() >= 0) || (rideTime.hour() < 6 && rideTime.minute() >= 0)) {
            pricePerFifthMile = (pricePerFifthMile + 0.5);
            this.logger.log(`Nigth time period pricePerFifthMile: ${pricePerFifthMile}`);
        }
        else {
            pricePerFifthMile = pricePerFifthMile;
            this.logger.log(`Standard period pricePerFifthMile: ${pricePerFifthMile}`);
        }
        return pricePerFifthMile;
    }
}
