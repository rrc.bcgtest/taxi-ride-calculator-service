export class Ride {

    id: number;
    distance: number;
    startTime: string;
    duration: number;

    constructor() { }
}
