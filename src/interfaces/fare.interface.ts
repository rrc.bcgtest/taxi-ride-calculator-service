import { Ride } from "./ride.interface";

export class Fare {

    ride: Ride;
    price: number;
    
    constructor() { }
}
