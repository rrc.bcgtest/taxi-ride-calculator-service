import { Module } from '@nestjs/common';
import { AppController } from './controllers/app.controller';
import { AppService } from './services/app.service';
import { Ride } from './interfaces/ride.interface';
import { FirebaseModule } from 'nestjs-firebase';
import { RideRepositoryService } from './repositories/ride-repository.service';
import { FareCalculatorService } from './services/fare-calculator.service';



@Module({
  imports: [
    FirebaseModule.forRoot({
      googleApplicationCredential: "firebase.json",
    }),
  ],
  controllers: [AppController],
  providers: [AppService, Ride, RideRepositoryService, FareCalculatorService],
  exports: [AppService]
})
export class AppModule { }
