import { Test, TestingModule } from '@nestjs/testing';
import { RideRepositoryService } from './ride-repository.service';

describe('RideRepositoryService', () => {
  let service: RideRepositoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RideRepositoryService],
    }).compile();

    service = module.get<RideRepositoryService>(RideRepositoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
