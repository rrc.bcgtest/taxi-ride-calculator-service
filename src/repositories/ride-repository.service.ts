import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { FirebaseAdmin, InjectFirebaseAdmin } from 'nestjs-firebase';
import { Ride } from 'src/interfaces/ride.interface';

@Injectable()
export class RideRepositoryService {
    private readonly logger = new Logger(RideRepositoryService.name);

    constructor(@InjectFirebaseAdmin() private readonly connection: FirebaseAdmin) { }

    /**
     * create a ride into database
     * @param ride 
     */
    async createRide(ride: Ride): Promise<Ride> {
        const doc = this.connection.db.collection('rides').doc(ride.id.toString());
        let result = await doc.set(JSON.parse(JSON.stringify(ride)));
        ride.id = Number(doc.id);
        this.logger.log(`Ride created with id ${doc.id}`);
        return ride;
    }

    /**
     * returns all Rides from database
     * @returns Promise<Ride[]>
     */
    async getAllRides(): Promise<Ride[]> {
        let rides: Ride[] = [];
        const docs = this.connection.db.collection('rides');
        const snapshot = await docs.get();
        snapshot.forEach(doc => {
            let ride: Ride = new Ride();
            let data = doc.data();
            ride = this.convert(data);
            rides.push(ride);
        });
        return rides;
    }

    /**
     * returns a ride by id
     * @param id 
     * @returns Promise<Ride>
     */
    async getRideById(id: number): Promise<Ride> {
        this.logger.log(`search ride with id ${id}`);
        let ride: Ride = new Ride();
        const doc = this.connection.db.collection('rides').doc(id.toString());
        const snapshot = await doc.get();
        const data = snapshot.data();
        if (!snapshot.exists) {
            throw new NotFoundException(`Ride ${id} Not Found`);
        }
        ride = this.convert(data);
        return ride;
    }

    /**
     * 
     * @param data 
     * @returns 
     */
    private convert(data: any): Ride {
        let ride: Ride = new Ride();
        ride.id = Number(data.id);
        ride.startTime = data.startTime;
        ride.duration = data.duration;
        ride.distance = data.distance;
        return ride;
    }

}
